version: "3"
services:
  postgresql:
    image: bitnami/postgresql:latest
    ports:
      - 5432:5432
    volumes:
      - postgresql_master_data:/bitnami/postgresql
    environment:
      POSTGRESQL_REPLICATION_MODE: "master"
      POSTGRESQL_REPLICATION_USER: "repl_user"
      POSTGRESQL_REPLICATION_PASSWORD: "repl_password"
      POSTGRESQL_USERNAME: "user"
      POSTGRESQL_PASSWORD: "password"
      POSTGRESQL_DATABASE: "data_store"
      POSTGRESQL_SYNCHRONOUS_COMMIT_MODE: "on"
      POSTGRESQL_NUM_SYNCHRONOUS_REPLICAS: 1
    networks:
      - rflow_rflow_overnet
    deploy:
      replicas: 2
      update_config:
        parallelism: 1
        delay: 10s
      restart_policy:
        condition: on-failure
  redis:
    image: 'docker.io/bitnami/redis:6.0-debian-10'
    ports:
      - 6379:6379
    volumes:
      - redis_data:/bitnami/redis/data
    environment:
      # ALLOW_EMPTY_PASSWORD is recommended only for development.
      ALLOW_EMPTY_PASSWORD: "yes"
    networks:
      - rflow_rflow_overnet
    deploy:
      replicas: 2
      update_config:
        parallelism: 1
        delay: 10s
      restart_policy:
        condition: on-failure
  airflow_webserver:
    image: 'apache/airflow:2.0.0-python3.8'
    ports:
      - 8080:8080
    volumes:
      - airflow_dags:/opt/airflow/dags
      - airflow_logs:/opt/airflow/logs
      - airflow_files:/opt/airflow/files
      - /var/run/docker.sock:/var/run/docker.sock
    environment:
      AIRFLOW__CORE__EXECUTOR: "CeleryExecutor"
      AIRFLOW__API__AUTH_BACKEND: "airflow.api.auth.backend.basic_auth"
      AIRFLOW__WEBSERVER__RBAC: "False"
      AIRFLOW__CORE__CHECK_SLAS: "False"
      AIRFLOW__CORE__STORE_SERIALIZED_DAGS: "False"
      AIRFLOW__CORE__PARALLELISM: 50
      AIRFLOW__CORE__LOAD_EXAMPLES: "False"
      AIRFLOW__CORE__LOAD_DEFAULT_CONNECTIONS: "False"
      AIRFLOW__SCHEDULER__SCHEDULER_HEARTBEAT_SEC: 10
      AIRFLOW__CELERY__BROKER_URL: "redis://:@redis:6379/0"
      AIRFLOW__CELERY__RESULT_BACKEND: "db+postgresql://user:password@postgres:5432/data_store"
      AIRFLOW__CORE__SQL_ALCHEMY_CONN: "postgresql+psycopg2://user:password@postgres:5432/data_store"
      AIRFLOW__CORE__FERNET_KEY: "Zt62766f9667ea_zt4e3Ziq3LdUUO7F2Z95cvFFx16hU8jTeR1ASM="
      AIRFLOW__CORE__PLUGINS_FOLDER: "/opt/airflow/plugins"
      AIRFLOW__WEBSERVER__RELOAD_ON_PLUGIN_CHANGE: "True"
    networks:
      - rflow_rflow_overnet
    deploy:
      replicas: 5
      update_config:
        parallelism: 1
        delay: 10s
      restart_policy:
        condition: on-failure
    command: webserver

  flower_celery:
    image: 'apache/airflow:2.0.0-python3.8'
    ports:
      - 5555:5555
    volumes:
      - airflow_logs:/opt/airflow/logs
    environment:
      AIRFLOW__CORE__EXECUTOR: "CeleryExecutor"
      AIRFLOW__API__AUTH_BACKEND: "airflow.api.auth.backend.basic_auth"
      AIRFLOW__WEBSERVER__RBAC: "False"
      AIRFLOW__CORE__CHECK_SLAS: "False"
      AIRFLOW__CORE__STORE_SERIALIZED_DAGS: "False"
      AIRFLOW__CORE__PARALLELISM: 50
      AIRFLOW__CORE__LOAD_EXAMPLES: "False"
      AIRFLOW__CORE__LOAD_DEFAULT_CONNECTIONS: "False"
      AIRFLOW__SCHEDULER__SCHEDULER_HEARTBEAT_SEC: 10
      AIRFLOW__CELERY__BROKER_URL: "redis://:@redis:6379/0"
      AIRFLOW__CELERY__RESULT_BACKEND: "db+postgresql://user:password@postgres:5432/data_store"
      AIRFLOW__CORE__SQL_ALCHEMY_CONN: "postgresql+psycopg2://user:password@postgres:5432/data_store"
      AIRFLOW__CORE__FERNET_KEY: "Zt62766f9667ea_zt4e3Ziq3LdUUO7F2Z95cvFFx16hU8jTeR1ASM="
      AIRFLOW__CORE__PLUGINS_FOLDER: "/opt/airflow/plugins"
      AIRFLOW__WEBSERVER__RELOAD_ON_PLUGIN_CHANGE: "True"
    networks:
      - rflow_rflow_overnet
    deploy:
      replicas: 2
      update_config:
        parallelism: 1
        delay: 10s
      restart_policy:
        condition: on-failure
    command: celery flower

  airflow_scheduler:
    image: 'apache/airflow:2.0.0-python3.8'
    volumes:
      - airflow_dags:/opt/airflow/dags
      - airflow_logs:/opt/airflow/logs
      - airflow_files:/opt/airflow/files
      - /var/run/docker.sock:/var/run/docker.sock
    environment:
      AIRFLOW__CORE__EXECUTOR: "CeleryExecutor"
      AIRFLOW__API__AUTH_BACKEND: "airflow.api.auth.backend.basic_auth"
      AIRFLOW__WEBSERVER__RBAC: "False"
      AIRFLOW__CORE__CHECK_SLAS: "False"
      AIRFLOW__CORE__STORE_SERIALIZED_DAGS: "False"
      AIRFLOW__CORE__PARALLELISM: 50
      AIRFLOW__CORE__LOAD_EXAMPLES: "False"
      AIRFLOW__CORE__LOAD_DEFAULT_CONNECTIONS: "False"
      AIRFLOW__SCHEDULER__SCHEDULER_HEARTBEAT_SEC: 10
      AIRFLOW__CELERY__BROKER_URL: "redis://:@redis:6379/0"
      AIRFLOW__CELERY__RESULT_BACKEND: "db+postgresql://user:password@postgres:5432/data_store"
      AIRFLOW__CORE__SQL_ALCHEMY_CONN: "postgresql+psycopg2://user:password@postgres:5432/data_store"
      AIRFLOW__CORE__FERNET_KEY: "Zt62766f9667ea_zt4e3Ziq3LdUUO7F2Z95cvFFx16hU8jTeR1ASM="
      AIRFLOW__CORE__PLUGINS_FOLDER: "/opt/airflow/plugins"
      AIRFLOW__WEBSERVER__RELOAD_ON_PLUGIN_CHANGE: "True"
    networks:
      - rflow_rflow_overnet
    deploy:
      replicas: 5
      update_config:
        parallelism: 1
        delay: 10s
      restart_policy:
        condition: on-failure
    command: scheduler
  
  airflow_worker_one:
    image: 'apache/airflow:2.0.0-python3.8'
    volumes:
      - airflow_dags:/opt/airflow/dags
      - airflow_logs:/opt/airflow/logs
      - airflow_files:/opt/airflow/files
      - /var/run/docker.sock:/var/run/docker.sock
    environment:
      AIRFLOW__CORE__EXECUTOR: "CeleryExecutor"
      AIRFLOW__API__AUTH_BACKEND: "airflow.api.auth.backend.basic_auth"
      AIRFLOW__WEBSERVER__RBAC: "False"
      AIRFLOW__CORE__CHECK_SLAS: "False"
      AIRFLOW__CORE__STORE_SERIALIZED_DAGS: "False"
      AIRFLOW__CORE__PARALLELISM: 50
      AIRFLOW__CORE__LOAD_EXAMPLES: "False"
      AIRFLOW__CORE__LOAD_DEFAULT_CONNECTIONS: "False"
      AIRFLOW__SCHEDULER__SCHEDULER_HEARTBEAT_SEC: 10
      AIRFLOW__CELERY__BROKER_URL: "redis://:@redis:6379/0"
      AIRFLOW__CELERY__RESULT_BACKEND: "db+postgresql://user:password@postgres:5432/data_store"
      AIRFLOW__CORE__SQL_ALCHEMY_CONN: "postgresql+psycopg2://user:password@postgres:5432/data_store"
      AIRFLOW__CORE__FERNET_KEY: "Zt62766f9667ea_zt4e3Ziq3LdUUO7F2Z95cvFFx16hU8jTeR1ASM="
      AIRFLOW__CORE__PLUGINS_FOLDER: "/opt/airflow/plugins"
      AIRFLOW__WEBSERVER__RELOAD_ON_PLUGIN_CHANGE: "True"
    networks:
      - rflow_rflow_overnet
    deploy:
      replicas: 5
      update_config:
        parallelism: 1
        delay: 10s
      restart_policy:
        condition: on-failure
    command: celery worker -H airflow_worker_one_name
  
  airflow_worker_two:
    image: 'apache/airflow:2.0.0-python3.8'
    volumes:
      - airflow_dags:/opt/airflow/dags
      - airflow_logs:/opt/airflow/logs
      - airflow_files:/opt/airflow/files
      - /var/run/docker.sock:/var/run/docker.sock
    environment:
      AIRFLOW__CORE__EXECUTOR: "CeleryExecutor"
      AIRFLOW__API__AUTH_BACKEND: "airflow.api.auth.backend.basic_auth"
      AIRFLOW__WEBSERVER__RBAC: "False"
      AIRFLOW__CORE__CHECK_SLAS: "False"
      AIRFLOW__CORE__STORE_SERIALIZED_DAGS: "False"
      AIRFLOW__CORE__PARALLELISM: 50
      AIRFLOW__CORE__LOAD_EXAMPLES: "False"
      AIRFLOW__CORE__LOAD_DEFAULT_CONNECTIONS: "False"
      AIRFLOW__SCHEDULER__SCHEDULER_HEARTBEAT_SEC: 10
      AIRFLOW__CELERY__BROKER_URL: "redis://:@redis:6379/0"
      AIRFLOW__CELERY__RESULT_BACKEND: "db+postgresql://user:password@postgres:5432/data_store"
      AIRFLOW__CORE__SQL_ALCHEMY_CONN: "postgresql+psycopg2://user:password@postgres:5432/data_store"
      AIRFLOW__CORE__FERNET_KEY: "Zt62766f9667ea_zt4e3Ziq3LdUUO7F2Z95cvFFx16hU8jTeR1ASM="
      AIRFLOW__CORE__PLUGINS_FOLDER: "/opt/airflow/plugins"
      AIRFLOW__WEBSERVER__RELOAD_ON_PLUGIN_CHANGE: "True"
    networks:
      - rflow_rflow_overnet
    deploy:
      replicas: 5
      update_config:
        parallelism: 1
        delay: 10s
      restart_policy:
        condition: on-failure
    command: celery worker -H airflow_worker_two_name
  
  airflow_initialize_db:
    image: 'apache/airflow:2.0.0-python3.8'
    volumes:
      - airflow_dags:/opt/airflow/dags
      - airflow_logs:/opt/airflow/logs
      - airflow_files:/opt/airflow/files
      - /var/run/docker.sock:/var/run/docker.sock
    environment:
      AIRFLOW__CORE__EXECUTOR: "CeleryExecutor"
      AIRFLOW__API__AUTH_BACKEND: "airflow.api.auth.backend.basic_auth"
      AIRFLOW__WEBSERVER__RBAC: "False"
      AIRFLOW__CORE__CHECK_SLAS: "False"
      AIRFLOW__CORE__STORE_SERIALIZED_DAGS: "False"
      AIRFLOW__CORE__PARALLELISM: 50
      AIRFLOW__CORE__LOAD_EXAMPLES: "False"
      AIRFLOW__CORE__LOAD_DEFAULT_CONNECTIONS: "False"
      AIRFLOW__SCHEDULER__SCHEDULER_HEARTBEAT_SEC: 10
      AIRFLOW__CELERY__BROKER_URL: "redis://:@redis:6379/0"
      AIRFLOW__CELERY__RESULT_BACKEND: "db+postgresql://user:password@postgres:5432/data_store"
      AIRFLOW__CORE__SQL_ALCHEMY_CONN: "postgresql+psycopg2://user:password@postgres:5432/data_store"
      AIRFLOW__CORE__FERNET_KEY: "Zt62766f9667ea_zt4e3Ziq3LdUUO7F2Z95cvFFx16hU8jTeR1ASM="
      AIRFLOW__CORE__PLUGINS_FOLDER: "/opt/airflow/plugins"
      AIRFLOW__WEBSERVER__RELOAD_ON_PLUGIN_CHANGE: "True"
    entrypoint: /bin/bash
    networks:
      - rflow_rflow_overnet
    deploy:
      replicas: 1
      update_config:
        parallelism: 1
        delay: 10s
      restart_policy:
        condition: on-failure
    command: -c "airflow db init && airflow users create --firstname rflow --lastname rflow --email rflow --password rflow --username rflow --role Admin"

  rflow_model_server:
    image: 'docker.io/bitnami/tensorflow-serving:2-debian-10'
    ports:
      - 8500:8500
      - 8501:8501
    volumes:
      - rflow_serving_data:/bitnami
      - rflow_model_data:/bitnami/model-data
    networks:
      - rflow_rflow_overnet
    deploy:
      replicas: 3
      update_config:
        parallelism: 1
        delay: 10s
      restart_policy:
        condition: on-failure

  rflow_visualizer:
    image: dockersamples/visualizer:stable
    ports:
      - 80:80
    stop_grace_period: 1m30s
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    deploy:
      placement:
        constraints: [node.role == manager]
    networks:
      - rflow_rflow_overnet
volumes:
  postgresql_master_data:
  redis_data:
  airflow_dagsff:
  airflow_logs:
  airflow_files:
  airflow_dags:
  rflow_serving_data:
  rflow_model_data: